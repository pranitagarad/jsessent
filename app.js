//1. Even Number function  1-10
document.write("<div style='width:50%;padding:10px;  box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;'>");

function evenNumber() {

    document.write("<h3>1.Print all even numbers from 0 – 10</h3>");

    for (var num = 0; num <= 10; num++) {
        if (num % 2 == 0) {
            console.log(num);

            document.write("<p>", num, "</p>");

        }
    }

}
evenNumber();
document.write("</div>");




//2. Print a table containing multiplication tables

document.write("<div style='width:50%;padding:10px; box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;'>");
function tables() {
    document.write("<h3>2. Print a table containing multiplication tables </h3>");
    document.write("<table>");
    for (num = 1; num <= 10; num++) {
        document.write("<tr>");
        for (i = 1; i <= 10; i++) {
            document.write("<td>");
            var result = i * num;

            console.log(result);
            document.write(result);
        }

    }
    document.write("</table>");

}
tables();
document.write("</div>");
document.write("<hr>");

//3.length converter function  kilometers to miles. 

document.write("<div style='width:50%;padding:10px;  box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;'>");
var numberkm = prompt("Enter Number to convert km to miles");

document.write("<h3>3. Create a length converter function </h3>");
document.write(" Entered number is :  ", numberkm);
function kmtomiles() {

    var result1 = numberkm / 1.609;

    document.write("<p> ", numberkm, " kilometer to Miles is ", result1, "</p>");
    console.log(numberkm, " kilometer to Miles ", result1);

}

kmtomiles();
document.write("</div>");
document.write("<hr>");

//4. Calculate the sum of numbers within an array

document.write("<div style='width:50%;padding:10px;  box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;'>");
var arr = [1, 10, 2, 12, 22, 5, 50, 15, 51]
console.log(arr.length);
var arrnum = 0;

document.write("<h3>4. Calculate the sum of numbers within an array</h3>");

document.write("Default array is : ", arr);

for (i = 0; i < arr.length; i++) {
    console.log("arr i", i);
    arrnum = arrnum + arr[i];

    console.log("Sum of array is : ", arrnum);



}
document.write("<p> Sum of array is : ", arrnum, "</p>");
document.write("</div>");
document.write("<hr>");

//5. Create a function that reverses an array

document.write("<div style='width:50%;padding:10px;  box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;'>");
function reverseArray() {
    document.write("<h3>5. Create a function that reverses an array</h3>");
    arra = [10, 11, 12, 13, 14, 15];
    document.write("Orignal array : ", arra);
    console.log(arra.length);
    for (i = 0; i < arra.length; i++) {
        var xyz = arra.length - i - 1;

        if (i < xyz) {
            var temp = arra[i];
            arra[i] = arra[xyz];
            arra[xyz] = temp;

        }

        console.log("value of", xyz);
    }
    console.log(arra);
    document.write("<p> Reversed Array : ", arra, "</p>")

}
reverseArray();
document.write("</div>");
document.write("<hr>");


//6. Sort an array from lowest to highest

document.write("<div style='width:50%;padding:10px; box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px; '>");

function sortArray() {
    document.write("<h3>6. Sort an array from lowest to highest</h3>");
    array = [5, 4, 3, 2, 1];
    console.log("orignal array", array);
    document.write("Orignal Array : ", array);

    for (i = 0; i < array.length; i++) {

        for (j = i; j < array.length; j++) {

            if (array[i] > array[j]) {
                var abc = array[j];
                array[j] = array[i];
                array[i] = abc;

            }
        }
    }
    console.log("lowest - highest ", array);
    document.write("<p>Lowest to highest : ", array, "</p>");
}
sortArray();
document.write("</div>");
document.write("<hr>");


//7. Create a function that filters out negative numbers

document.write("<div style='width:50%;padding:10px; box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;'>");
document.write("<h3>7. Create a function that filters out negative numbers</h3>");
var neg = [10, 15, -10, 5, -54];
document.write("Orignal Array is : [ ", neg, " ] ");

function negativeNumb(Arr) {

    var temp = [];
    for (i = 0; i < Arr.length; i++) {
        if (Arr[i] >= 0) {
            temp.push(Arr[i]);


            console.log("Arr of i ", Arr[i]);


        }
    }
    return temp;
}
var result = negativeNumb(neg);
console.log(result);
document.write("<p> Filter Array is : [ ", result, "] </p>");
document.write("</div>");
document.write("<hr>");

//8. Remove the spaces found in a string

document.write("<div style='width:50%;padding:10px; box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;'>");
document.write("<h3> 8. Remove the spaces found in a string</h3>");
var spacestring = " Remove the spaces found in a string "
document.write("Thw given String is : ", spacestring);
function findSpaces() {

    document.write("<p> Replaced String is : ", spacestring.replace(/\s/g,''), "</p>");
}
findSpaces();
document.write("</div>");
document.write("<hr>");



//9.  Return a Boolean if a number is divisible by 10

document.write("<div style='width:50%;padding:10px;box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;'>");
var fno = prompt("Enter the number to check it is dividible by 10 ");
document.write("<h3> 9. Return a Boolean if a number is divisible by 10</h3>");
function returnBoolean() {
    document.write("Entered Number is : ", fno)
    if (fno % 10 == 0) {
        console.log(true)
        document.write(true)

    }
    else {
        console.log(false);
        document.write("<p> Result : ", false, "</p>")
    }
}
returnBoolean();
document.write("</div>");
document.write("<hr>");


//10. Return the number of vowels in a string

document.write("<div style='width:50%;padding:10px; box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;' >");
document.write("<h3> 10. Return the number of vowels in a string</h3>");
var stringmeasure = "Return the number of vowels in a string";
document.write("Orignal string is : ", stringmeasure);
function vowelsString(str) {
    var strcount = 0;
    var vowels = ['a', 'e', 'i', 'o', 'u'];
    for (i = 0; i < str.length; i++) {
        if (str[i] == 'a' || str[i] == 'e' || str[i] == 'i' || str[i] == 'o' || str[i] == 'u' || str[i] == 'A' || str[i] == 'E' || str[i] == 'I' || str[i] == 'O' || str[i] == 'U') {
            strcount += 1;
        }
    }
    console.log("vowels Count", strcount);
    document.write("<p> Vowels Count is : ", strcount, "</p>");
}
vowelsString(stringmeasure);
document.write("</div");
document.write("<hr>");



